#pragma once

#include <string>
#include <stdint.h>

#include "types.h"

namespace W {

struct ARBG{
	ARBG(uint32_t color);
	std::string toString();
	uint32_t raw;//!ARBG
	uint8_t r,g,b;//!unpacked
};

ARBG getPixel(Point p);

};
