#include "winpixel.h"

#include <sstream>
#include <windows.h>
#include <wingdi.h>
#include <winuser.h>

auto dc=GetDC(nullptr);
W::ARBG W::getPixel(Point p){
	ARBG color = GetPixel(dc, p.x, p.y);
	return color;
}

W::ARBG::ARBG(uint32_t color):raw(color){
	r = GetRValue(color);
	g = GetGValue(color);
	b = GetBValue(color);
}

std::string W::ARBG::toString(){
	std::stringstream ss;
	ss << " r: " << int(r) << " g: " << int(g) << " b: " << int(b);
	return ss.str();
}
