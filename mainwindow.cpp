#include "mainwindow.h"

#include <QEvent>
#include <QApplication>
#include <QStyle>
#include <QMenu>
#include <QTime>
#include <QGraphicsDropShadowEffect>
#include <QThread>
#include <QSpinBox>
#include <QScreen>
#include <QPainter>
#include <QPointer>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QLabel>
#include <QSystemTrayIcon>
#include <QTimer>

#include <iostream>
#include <Win32CPP/winpixel.h>

namespace M {

struct Macro: public Macro_I{
	Macro (QWidget& parent):parent(parent){}
	~Macro(){stop();}
	void handleInputEvent(W::Input::Event e) override{
		if(e.type==W::Input::Event::Type::KeyDown && e.key==W::Key::INSERT){
			pause=!pause;
		}
		if(e.key==triggerKey()){
			if(e.type==W::Input::Event::Type::KeyDown){
				triggerKeyPressed();
			}else{
				triggerKeyReleased();
			}
		}
	}
	void doPause() override{
		pause=true;
	}
protected:
	bool pause=false;//! disable tick() from being called;
	QWidget& parent;//! a widget from wich to be child to render on screen

	virtual W::Key triggerKey()=0;
	virtual void triggerKeyPressed()=0;
	virtual void triggerKeyReleased()=0;

	//! tick called in the thread loop
	virtual void tick()=0;

	//! activate or deactivate the thread loop
	void toogle(){
		if(!loopThread){
			start();
		}else{
			stop();
		}
	}

	//! activate the thread loop
	void start(){
		if(!loopThread){
			loopThread=QThread::create([this](){
				while (!loopThread->isInterruptionRequested()) {
					if(!pause){
						tick();
					}else{
						QThread::msleep(10);
					}
				}
			});
			QObject::connect(loopThread,&QThread::finished,loopThread,[this](){
				loopThread->deleteLater();
			});
			loopThread->start();
		}
	}

	//! deactivate the thread loop
	void stop(){
		if(loopThread){
			loopThread->requestInterruption();
		}
	}

	bool active() override{
		return loopThread;
	}
private:
	QPointer<QThread> loopThread;
};

struct ToogleMacro:public Macro {
	using Macro::Macro;
	void triggerKeyPressed() override{toogle();}
	void triggerKeyReleased() override{}
	void handleInputEvent(W::Input::Event e) override{
		Macro::handleInputEvent(e);
		if(pause)stop();
	}

};
struct PressMacro:public Macro {
	using Macro::Macro;
	void triggerKeyPressed() override{start();}
	void triggerKeyReleased() override{stop();}
};
struct OneShootMacro:public Macro {
	using Macro::Macro;
	void triggerKeyPressed() override{if(!pause){tick();}}
	void triggerKeyReleased() override{}
};

}

template<class Widget>
QPointer<Widget> allocateToLayout(QPointer<QBoxLayout> ly,Widget* wg){
	ly->addWidget(wg);
	return wg;
}
struct KeySelectWidgets{
	KeySelectWidgets(QWidget& parent, QPoint pos){
		root=new QWidget(&parent);
		root->setStyleSheet("QCheckBox { color : cyan ; background-color : rgba(0, 0, 0, 0.4); }");
		root->move(pos);

		auto verticalLayout=new QVBoxLayout(root);
		verticalLayout->setSpacing(0);
		verticalLayout->setMargin(0);

		a =   allocateToLayout(verticalLayout, new QCheckBox(  "A"));
		z =   allocateToLayout(verticalLayout, new QCheckBox(  "Z"));
		e =   allocateToLayout(verticalLayout, new QCheckBox(  "E"));
		r =   allocateToLayout(verticalLayout, new QCheckBox(  "R"));
		tab = allocateToLayout(verticalLayout, new QCheckBox("TAB"));
		t =   allocateToLayout(verticalLayout, new QCheckBox(  "T"));

		root->show();
	}
	void triggerSelected(){
		if(t  ->isChecked()) W::pressKey(W::Key::T);
		if(a  ->isChecked()) W::pressKey(W::Key::A);
		if(z  ->isChecked()) W::pressKey(W::Key::Z);
		if(e  ->isChecked()) W::pressKey(W::Key::E);
		if(r  ->isChecked()) W::pressKey(W::Key::R);
		if(tab->isChecked()) W::pressKey(W::Key::TAB);
	}
	QPointer<QCheckBox> a,z,e,r,tab,t;
	QPointer<QWidget> root;
};

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent){
	setStyleSheet("background:transparent;");
	setAttribute(Qt::WA_TranslucentBackground);
	setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint/*|Qt::ToolTip*/);
	centralwidget = new QWidget(this);
	setCentralWidget(centralwidget);

	connect(&qi,&QInput::handleEventThreadSafe,[=](W::Input::Event e){
		for (auto& macro : macros) {
			macro->handleInputEvent(e);
		}
	});

	auto appContextMenu=[this](){
		QMenu m;
		connect(m.addAction("Exit"),&QAction::triggered,[]{QApplication::quit();});
		connect(m.addAction("Pause"),&QAction::triggered,[this]{
			for (auto& macro : macros) {
				macro->doPause();
			}
		});
		m.exec(QCursor::pos());
	};

	/*tray icone*/{
		auto trayIcon=new QSystemTrayIcon(this);
		trayIcon->setIcon(QApplication::style()->standardIcon(QStyle::SP_MessageBoxCritical));
		trayIcon->setToolTip("Clock");
		trayIcon->setVisible(true);
		connect(trayIcon,&QSystemTrayIcon::activated,appContextMenu);
	}
	/*clock*/{
		QLabel* label = new QLabel(centralwidget);

		label->setStyleSheet("QLabel { color : cyan ; background-color : rgba(0, 0, 0, 0.4); }");
		label->setGeometry(0,-8,120,50);
		label->setAlignment(Qt::AlignLeft|Qt::AlignTop);

		QFont font;{
			font.setFamily(QString::fromUtf8("Gadugi"));
			font.setPointSize(30);
		}
		label->setFont(font);

		label->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
		connect(label,&QWidget::customContextMenuRequested,appContextMenu);

		/*timer updating and drawing*/{
			auto updateRender=[label]{
				label->setText(
							/*must use html or reimplement paint event ....*/
							QString("<font color=\"cyan\">%1</font>").arg(QTime::currentTime().toString("hh:mm"))
							);
			};

			updateRender();

			auto t=new QTimer(centralwidget);
			t->setSingleShot(false);
			t->setInterval(1000*30);
			connect(t,&QTimer::timeout,updateRender);
			t->start();
		}
	}

	/*LClick_press_RightClickSpam & M5_tootle_Spam*/{
		struct M5_toogle_Spam:public M::ToogleMacro {
			KeySelectWidgets selector;
			QPointer<QWidget> activeIndicator;

			M5_toogle_Spam(QWidget& parent):M::ToogleMacro(parent),selector(parent,{3,42}){
				selector.t->setChecked(true);
				selector.a->setChecked(true);
				selector.z->setChecked(true);
				selector.e->setChecked(true);


				activeIndicator=new QLabel("Active",&parent);
				activeIndicator->setStyleSheet("QLabel { color : cyan ; background-color : rgba(255, 0, 0, 0.9); }");
				activeIndicator->move({0,162});
				activeIndicator->show();
				activeIndicator->setVisible(active());
			}

			void handleInputEvent(W::Input::Event e) override{
				M::ToogleMacro::handleInputEvent(e);
				selector.root->setVisible(!pause);
				activeIndicator->setVisible(active());
			}

			W::Key triggerKey() override{return W::Key::mouse_XBUTTON2;}
			void tick() override{
				if(pause){ return; }

				QThread::msleep(60);
				/*manualy pressing a key that is not spamed disable the macro*/{
					if(!selector.a->isChecked() && W::Input::keyPressed(W::Key::A)){ return; }
					if(!selector.z->isChecked() && W::Input::keyPressed(W::Key::Z)){ return; }
					if(!selector.e->isChecked() && W::Input::keyPressed(W::Key::E)){ return; }
					if(!selector.r->isChecked() && W::Input::keyPressed(W::Key::R)){ return; }
				}
				selector.triggerSelected();
			}
		};
		macros.push_back(std::make_unique<M5_toogle_Spam>(*centralwidget));
		struct LClick_press_RightClickSpam:public M::PressMacro {
			KeySelectWidgets selector;
			Macro_I& activator;

			LClick_press_RightClickSpam(QWidget& parent, Macro_I& activator)
				:M::PressMacro(parent)
				,selector(parent,{50,42})
				,activator(activator)
			{
				selector.r->setChecked(true);
			}
			using M::PressMacro::PressMacro;
			W::Key triggerKey() override{return W::Key::mouse_LBUTTON;}
			void handleInputEvent(W::Input::Event e) override{
				M::PressMacro::handleInputEvent(e);
				selector.root->setVisible(!pause);

				if (e.pos) std::cout << e.pos->toString() << std::endl;
			}
			void tick() override{
				if(pause){ return; }
				if(!activator.active()){ return; }
				QThread::msleep(50);
				if(W::getPixel({1328,1234}).g>60){
					selector.triggerSelected();
					W::pressKey(W::Key::Q);
				}else{
					W::pressKey(W::Key::Q);
				}
			}
		};
		macros.push_back(std::make_unique<LClick_press_RightClickSpam>(*centralwidget,*macros.back()));
	}
	/*Midle_press_RightClickSpam*/{
		struct Midle_press_RightClickSpam:public M::PressMacro {
			using M::PressMacro::PressMacro;
			W::Key triggerKey() override{return W::Key::mouse_MBUTTON;}
			void tick() override{
				W::pressKey(W::Key::mouse_RBUTTON);
				QThread::msleep(50);
			}
		};
		macros.push_back(std::make_unique<Midle_press_RightClickSpam>(*centralwidget));
	}
	/*AutoMisticUtils*/{
		struct AutoMisticUtils {
			enum class MisticPos{_1,_2,_3};
			static void reroll(MisticPos pos){
				static constexpr Point rerollPos={360,1040};
				Point choicePos;
				switch (pos) {
				case MisticPos::_1: choicePos={rerollPos.x,525}; break;
				case MisticPos::_2: choicePos={rerollPos.x,590}; break;
				case MisticPos::_3: choicePos={rerollPos.x,650}; break;
				}

				constexpr auto sleep=100;

				W::mouseMoveClick(choicePos);
				QThread::msleep(sleep);
				W::mouseMoveClick(rerollPos);
				QThread::msleep(sleep);
				W::mouseMoveClick(rerollPos);
				QThread::msleep(sleep);
			}
		};
		struct AutoMistic1:public M::OneShootMacro {
			using M::OneShootMacro::OneShootMacro;
			W::Key triggerKey() override{return W::Key::NUMPAD7;}
			void tick() override{
				AutoMisticUtils::reroll(AutoMisticUtils::MisticPos::_1);
			}
		};
		struct AutoMistic2:public M::OneShootMacro {
			using M::OneShootMacro::OneShootMacro;
			W::Key triggerKey() override{return W::Key::NUMPAD4;}
			void tick() override{
				AutoMisticUtils::reroll(AutoMisticUtils::MisticPos::_2);
			}
		};
		struct AutoMistic3:public M::OneShootMacro {
			using M::OneShootMacro::OneShootMacro;
			W::Key triggerKey() override{return W::Key::NUMPAD1;}
			void tick() override{
				AutoMisticUtils::reroll(AutoMisticUtils::MisticPos::_3);
			}
		};
//		macros.push_back(std::make_unique<AutoMistic1>(*centralwidget));
//		macros.push_back(std::make_unique<AutoMistic2>(*centralwidget));
//		macros.push_back(std::make_unique<AutoMistic3>(*centralwidget));
	}
	/*RarityOverlay*/{
		struct RarityOverlay:public M::Macro{
			const std::string qualityPropertyName="BestQuality";
			//full custom macro only use the pause fonctionality from base Macro

			W::Key triggerKey() override {return {};}
			void tick() override {}
			void triggerKeyPressed() override {}
			void triggerKeyReleased() override {}

			std::map<Point,QPointer<QLabel>> icons;
			std::vector<Point> pikingPoses;

			RarityOverlay(QWidget& parent):M::Macro(parent){
				constexpr float countX=10;;
				constexpr float countY=6;
				constexpr float offsetX=605/(countX-1);
				constexpr float offsetY=333/(countY-1);
				constexpr int up=747;
				constexpr int right=1873;
				for (int y = 0; y < countY; ++y) {
					for (int x = 2; x < countX; ++x) {
						int _x=right+x*offsetX;
						int _y=up+y*offsetY;
						pikingPoses.push_back({_x,_y});
						auto& ico=icons[pikingPoses.back()];
						ico=new QLabel("",&parent);
						ico->setVisible(false);
						ico->move({_x+45,_y+45});
					}
				}
				std::reverse(pikingPoses.begin(),pikingPoses.end());

				hideAllIcons();

				for (auto& [_,ico] : icons) {
					ico->setContextMenuPolicy(Qt::ContextMenuPolicy::CustomContextMenu);
					connect(ico,&QWidget::customContextMenuRequested,[this](){
						hideAllIcons();
					});
				}
			}

			void hideAllIcons(){
				for (auto& [_,ico] : icons) {
					ico->setVisible(false);
				}
			}

			void handleInputEvent(W::Input::Event e) override {
				M::Macro::handleInputEvent(e);

				if(e.type==W::Input::Event::Type::KeyDown){

					if(e.key==W::Key::ESCAPE || e.key==W::Key::NUMPAD0){
						hideAllIcons();
					}

					if(!pause){
						if(e.key == W::Key::DELETE_k){
							for (auto& [_,ico] : icons) {
								if(ico->isVisible() && !ico->property(qualityPropertyName.c_str()).toBool()){
									W::mouseMoveClick({ico->pos().x()-10,ico->pos().y()});
									QThread::msleep(75);
									W::pressKey(W::Key::RETURN);
									QThread::msleep(75);
								}
							}
						}
						if(e.key==W::Key::RIGHT){
							decltype (pikingPoses) selected;/*determine legendary poses with a screenshoot*/{
								auto screenshot = QGuiApplication::primaryScreen()->grabWindow(0).toImage();
								for (auto& [x,y] : pikingPoses) {
									icons.at({x,y})->setProperty(qualityPropertyName.c_str(),false);
									auto col=screenshot.pixelColor(x+53,y+53);
									if(col.red()>200 || col.green()>200 || col.green()==0/*overlay*/){
										selected.push_back({x,y});
									}
								}
							}

							for (auto& [x,y] : selected) {
								bool isTheBestQuality=false;{
									W::mouseMove({x+10,y+10});

									QThread::msleep(2);/*avoid false positive based on popup not having time to show yet*/
									for (int i = 0; i < 2; ++i)/*we "wait" longer if there is no hit to be sure that the GUI had time to render*/{
										QThread::msleep(1);
										auto col=W::getPixel({x-6,y+30}/*look near the midle of the cell to avoid the top borders*/);
										if(col.r>30){
											isTheBestQuality=true;
											break;
										}
									}
								}

								auto& ico=icons.at({x,y});
								QImage i(18,18,QImage::Format::Format_ARGB32);
								QPainter p(&i);
								i.fill(Qt::transparent);

								if(isTheBestQuality){
									p.setBrush(Qt::yellow);
									ico->setProperty(qualityPropertyName.c_str(),true);
								}else{
									p.setBrush(Qt::black);
								}
								p.setRenderHint(QPainter::Antialiasing);
								p.drawEllipse(0,0,i.width(),i.height());
								ico->setPixmap(QPixmap::fromImage(i));
								ico->setVisible(true);
							}
							W::mouseMove({pikingPoses.back().x-5,pikingPoses.back().y});
						}}

				}
			}
		};
		//macros.push_back(std::make_unique<RarityOverlay>(*centralwidget));
	}
}
