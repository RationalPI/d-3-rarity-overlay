function(deployQtTarget targetName outputPath)
    find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core REQUIRED)
    set(winDeploy "${_qt5Core_install_prefix}/bin/windeployqt.exe")
    if (EXISTS ${winDeploy})
        message(STATUS "windeployqt found: \"${winDeploy}\"")
    else()
        message(FATAL_ERROR "windeployqt not found start by making your app build before trying to deploy it, it could help ...")
    endif()

    add_custom_command(TARGET ${targetName} POST_BUILD
        COMMAND ${winDeploy} --dir ${outputPath} $<TARGET_FILE:${targetName}>
        )
    add_custom_command(TARGET ${targetName} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${targetName}> ${outputPath}/
        )
endfunction()
