#pragma once

#include <Win32CPP/wininput.h>
#include <QObject>

Q_DECLARE_METATYPE(W::Input::Event)
struct QInput: public QObject{
	QInput();
	static Point currentPos();
	Q_OBJECT
signals:
	void handleEvent(W::Input::Event e);
	void handleEventThreadSafe(W::Input::Event e);
private:
	W::Input kh;
};
